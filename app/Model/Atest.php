<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Atest extends Model
{
    public function children(): HasMany
    {
        // The order_no field is used to determine the order of child items within each parent node
        return $this->hasMany(__CLASS__,'pef_item_id','id')->orderBy('order_no');
    }
     public function ancestor(): BelongsTo
     {
       return $this->belongsTo(__CLASS__,'pef_item_id','id');
     }
}
