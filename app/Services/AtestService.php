<?php
 namespace App\Services;
 
use App\Model\Atest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class AtestService{
    public function getTree(Request $request): Collection
    {
        /** @var Collection $children */
        $children =  Atest::all();
        if ($request->has('text')) {
            $texts = $request->text;

            $children = $children->filter(  // return display nodes that contain the given string
                static function (Atest $atest) use ($texts) {

                    return Str::contains(Str::lower($atest->name), Str::lower($texts));
                }
            )->map(
            //  return ancestor ancestor nodes (irrespective of whether the ancestor nodes contain the string or not)
                static function (Atest $atest) use ($texts) {
                    /** @var Collection $testChildren */
                    $testChildren = $atest->ancestor->children->filter(
                        static function ($q) use ($texts) {
                            return Str::contains(Str::lower($q->name), Str::lower($texts)); //return item if it contain text via filter
                        }
                    );
                    if (!$testChildren->count()) {
                        // but NOT the child nodes of the ancestor nodes that do not contain the string
                        $atest->ancestor->children->reject(
                            static function (){
                                return true;
                            }
                        );
                    }

                    return $atest; //if children contains texts, return the whole thing

                }
            )->filter(
            /**
             * This works as of PHP 7.1| either we receive an instance of Atest or null
             */
                static function (?Atest $atest) {

                    return $atest !== null;
                }
            );
        }

        return $children;
    }
}