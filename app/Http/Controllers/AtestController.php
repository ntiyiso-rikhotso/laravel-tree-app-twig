<?php

namespace App\Http\Controllers;

use App\Model\Atest;
use App\Services\AtestService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AtestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index(): View
    {
        return view('index');
    }

    /**
     * @param Request $request
     *
     * @return Factory|View
     */
    public function displayTree(Request $request)
    {
        $children = (new AtestService)->getTree($request);

        return view('partial', compact('children'));
    }
}
