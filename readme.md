## Laravel Tree App with Twig

I will be using Laravel framework integrated with twig templating engine to build a tree app

## Requirements 
 - PHP >= 7.2.5
 - BCMath PHP Extension
 - Ctype PHP Extension
 - Fileinfo PHP extension
 - JSON PHP Extension
 - Mbstring PHP Extension
 - OpenSSL PHP Extension
 - PDO PHP Extension
 - Tokenizer PHP Extension
 - XML PHP Extension
 - I.e Just make sure that you have an environment that can run a Laravel application

## Installation
1. Open `.env` file and change database configuration. Please start by creating a database named `db` to ease things
```
DB_HOST=db
DB_PORT=3306
DB_DATABASE=db
DB_USERNAME=db
DB_PASSWORD=db
```

When you have completed the previous step and you successfully connected to the database


## Config
- open terminal and run the following commands in the root directory
 - `$ composer install`
 - `$ composer dump-autoload`
 - `$ php artisan serve`  //this will give you the url to open
 - `$ php artisan migrate`
 
 You should be able to run the app
 
 





