require('./bootstrap');

$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    }
  });
  let displayTree = function(text = null) {
    data = {};
    if(text){
      data.text = text;
    }
    $.get('/display-tree', data,function(html) {
      $('#tree-structure').html(html);
    });
  };
  $(document)
  .ready(function() {
      $('#display-tree').click()
  })    
  .on('click', '.list-group-item', function() {
    $('.glyphicon', this).
        toggleClass('glyphicon-chevron-right').
        toggleClass('glyphicon-chevron-down');
  })
  .on('click', '#display-tree', function() {
    displayTree();
  })
  .on('click', '#trigger-search', function(a) {
    a.preventDefault();
    let text = $('#text').val();
    displayTree(text);
  })
  .on('keypress keydown keyup', function(k) {
    let input = $('#text');
    let code = k.keyCode || k.which;
    let isEnterKey =  code === 13;
    let isFocusInput = input.is(":focus");
    let value = input.val();
    let isMoreThan2Chars = value.length > 2;
      if((isEnterKey && isFocusInput) || isMoreThan2Chars){
        displayTree(value);
      }
  });

});







