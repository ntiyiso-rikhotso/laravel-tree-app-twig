<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PagesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHomePageIsAccessible()
    {

        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testRouteDisplayTreeWorks()
    {

        $response = $this->get('/display-tree');

        $response->assertStatus(200);
    }
    public function testIfUnknownRouteReturns404()
    {

        $response = $this->get('/unknown-random-route');
        $response->assertStatus(404);
    }
    public function testAtestRoute()
    {

        $response = $this->get('/atest');
        $response->assertDontSee('Please click');
        $response->assertStatus(404);
    }

    public function testAtestRouteWithAdditionalParameter()
    {

        $response = $this->get('/atest/12');
        $response->assertStatus(404);
    }
}
