<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataToTableAtest extends Migration
{
    private static $table = 'atests';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $file = file_get_contents(__DIR__ . '/atest-data.sql');
        DB::raw($file);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
        Schema::create(self::$table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 60);
            $table->integer('pef_item_id');
            $table->integer('order_no');
        });
    }
}
